$(document).ready(function () {
  $("#btnregistrar").click(function (e) {
    e.preventDefault();
    registrar();
  });

  /*
  $('.onclick-eliminar').click(function (e) {

    // Consultar antes de eliminar
    if(!confirm("¿Desea eliminar este registro?"))
    {
      return false;
    }

    // Eliminar registro
    var fila = $(this).parents().get(1);
    fila.remove();
    
  });
  */

  $('body').on('click', '.onclick-eliminar', function (e) {

    // Consultar antes de eliminar
    if (!confirm("¿Desea eliminar este registro?")) {
      return false;
    }

    // Eliminar registro
    var fila = $(this).parents().get(1);
    fila.remove();

    // Mensaje de confirmación
    swal.fire('Excelente', 'Registro eliminado', 'success');


  });

});

function registrar() {
  // Obtener datos
  var nombre = $("#txtnombre").val();
  var correo = $("#txtcorreo").val();
  var fecnac = $("#txtfecha").val();
  var pais = $("#cbopais").val();
  var sexo = $("input:radio[name=chksexo]:checked").val();

  // Agregar a tabla
  var fila = ''
  fila += '<tr>';
  fila += '<td>' + nombre + '</td>';
  fila += '<td>' + correo + '</td>';
  fila += '<td>' + fecnac + '</td>';
  fila += '<td>' + pais + '</td>';
  fila += '<td>' + sexo + '</td>';
  fila += '<td>' + '<button class="btn btn-danger btn-sm onclick-eliminar"> <span class="fa fa-times"></span> </button>' + '</td>';
  fila += '</tr>';

  $("#tbldatos").append(fila);

  // Mostrar mensaje de confirmacion
  swal.fire('Excelente', 'Registro exitoso', 'success');


  // Ver datos en consola
  console.log("Nombre: " + nombre);
  console.log("Correo: " + correo);
  console.log("Fecha nacimiento: " + fecnac);
  console.log("País: " + pais);
  console.log("Sexo: " + sexo);
}